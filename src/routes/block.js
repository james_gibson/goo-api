 //Set up Mongo
if(process.env.VCAP_SERVICES){
  var env = JSON.parse(process.env.VCAP_SERVICES);
  var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
  var mongo = {
    "hostname":"localhost",
    "port":27017,
    "username":"",
    "password":"",
    "name":"",
    "db":"db"
  }
}

var generate_mongo_url = function(obj){
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'test');

  if(obj.username && obj.password){
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
  else{
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
}

var mongourl = generate_mongo_url(mongo);




 /*
  *	POST block by x,y,z
  */

exports.add = function(req, res){

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.insert( req.body, {safe:true}, function(err){
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end({inserted: req.body.length});
	    });
	  });
	});
}

/*
 * GET blocks listing.
 */

exports.list = function(req, res){
  require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find(function(err, cursor) {
			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify(items));
			});
	    });
	  });
	});
};


/*
 * 	GET block by ID
 * 	/blocks/:block_id
 */

 exports.findById = function(req, res){

 	var ObjectID = require('mongodb').ObjectID;

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.findOne({'_id':new ObjectID(req.params.block_id)}, function(err, document) {
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(document));
	    });
	  });
	});
 }


/*
 * 	GET block by PlayerId
 * 	/players/:player_id/blocks
 */

 exports.findByPlayerId = function(req, res){
 	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find({playerId:req.params.player_id}, function(err, cursor) {
			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify(items));
			});
	    });
	  });
	});
 }


/*
 * 	GET block summary by PlayerId
 * 	/players/:player_id/blocks/summary
 */

 exports.summaryByPlayerId = function(req, res){
 	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find({playerId:req.params.player_id}, function(err, cursor) {


			cursor.toArray(function(err, items) {

                var input = items;
				var count = input.length;
                var result = {};

                for(var i = 0; i < input.length; i++)
                {
                    if(typeof result[input[i].finalId] === 'undefined')
                    {
                        result[input[i].finalId] =   {placed : 0, destroyed : 0};
                        //console.log(result[input[i].finalId]);

                    }

                        if (input[i].initialId != 0)
                        {
                            result[input[i].finalId].destroyed =   result[input[i].finalId].destroyed + 1;
                        }
                        else
                        {
                            result[input[i].finalId].placed =   result[input[i].finalId].placed + 1;
                        }

                        //console.log(input[i],result[input[i].finalId]);



                }

                /*var results = [];
                function summarizeBlockRecords(element, index, array) {
                    //console.log(JSON.stringify(element));
                    results[element.finalId]= 1;


                }
                items.forEach(summarizeBlockRecords); */


                res.writeHead(200, {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                });
				res.end(JSON.stringify({blockSummary: result, count: count}));
			});
	    });
	  });
	});
 }
