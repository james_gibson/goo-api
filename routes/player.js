//Set up Mongo
if(process.env.VCAP_SERVICES){
  var env = JSON.parse(process.env.VCAP_SERVICES);
  var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
  var mongo = {
    "hostname":"localhost",
    "port":27017,
    "username":"",
    "password":"",
    "name":"",
    "db":"db"
  }
}

var generate_mongo_url = function(obj){
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'test');

  if(obj.username && obj.password){
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
  else{
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
}

var mongourl = generate_mongo_url(mongo);

/*
 * GET players listing.
 */

exports.list = function(req, res){
  require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('players', function(err, coll){
	    coll.find(function(err, cursor) {
			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify(items));
			});
	    });
	  });
	});
};

/*
 * 	GET player by playerID
 * 	/players/:player_id
 */

 exports.findByPlayerName = function(req, res){

 	var ObjectID = require('mongodb').ObjectID;

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('players', function(err, coll){
	    coll.findOne({'ign':req.params.player_id}, function(err, document) {
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			document.blocks_url = "http://localhost:3001/api/players/" + req.params.player_id + '/blocks';
			res.end(JSON.stringify(document));
	    });
	  });
	});
 }

 exports.findById = function(req, res){

 	var ObjectID = require('mongodb').ObjectID;

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('players', function(err, coll){
	    coll.findOne({'_id':new ObjectID(req.params.id)}, function(err, document) {
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(document));
	    });
	  });
	});
 }


 /*
  *	POST player by Id
  */

exports.add = function(req, res){

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('players', function(err, coll){
	    coll.insert( req.body, {safe:true}, function(err){
	    	console.log("Adding")
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(req.body));
	    });
	  });
	});
}
