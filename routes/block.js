 //Set up Mongo
if(process.env.VCAP_SERVICES){
  var env = JSON.parse(process.env.VCAP_SERVICES);
  var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
  var mongo = {
    "hostname":"localhost",
    "port":27017,
    "username":"",
    "password":"",
    "name":"",
    "db":"db"
  }
}

var generate_mongo_url = function(obj){
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'test');

  if(obj.username && obj.password){
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
  else{
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
}

var mongourl = generate_mongo_url(mongo);




 /*
  *	POST block by x,y,z
  */

exports.add = function(req, res){

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.insert( req.body, {safe:true}, function(err){
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(req.body));
	    });
	  });
	});
}

/*
 * GET blocks listing.
 */

exports.list = function(req, res){
  require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find(function(err, cursor) {
			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify(items));
			});
	    });
	  });
	});
};


/*
 * 	GET block by ID
 * 	/blocks/:block_id
 */

 exports.findById = function(req, res){

 	var ObjectID = require('mongodb').ObjectID;

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.findOne({'_id':new ObjectID(req.params.block_id)}, function(err, document) {
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(document));
	    });
	  });
	});
 }


/*
 * 	GET block by PlayerId
 * 	/players/:player_id/blocks
 */

 exports.findByPlayerId = function(req, res){
 	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find({playerId:req.params.player_id}, function(err, cursor) {
			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify(items));
			});
	    });
	  });
	});
 }


/*
 * 	GET block summary by PlayerId
 * 	/players/:player_id/blocks/summary
 */

 exports.summaryByPlayerId = function(req, res){
 	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	    coll.find({playerId:req.params.player_id}, function(err, cursor) {
			

			cursor.toArray(function(err, items) {
				res.writeHead(200, {
				  "Content-Type": "application/json",
				  "Access-Control-Allow-Origin": "*"
				});
				res.end(JSON.stringify({value:items.length}));
			});
	    });
	  });
	});
 }


 /*
  *	PUT block array dumps
  */

exports.stuffItIn = function(req, res){

	require('mongodb').connect(mongourl, function(err, conn){
	  conn.collection('blocks', function(err, coll){
	  	JSON.parse(req.body).forEach(function(item, index) {
  			console.log(item);// `item` is the next item in the array
  			// `index` is the numeric position in the array, e.g. `array[index] == item`
		});
	    coll.insert( req.body, {safe:true}, function(err){
			res.writeHead(200, {
			  "Content-Type": "application/json",
			  "Access-Control-Allow-Origin": "*"
			});
			res.end(JSON.stringify(JSON.parse(req.body)));
	    });
	  });
	});
}
