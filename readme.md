## Goo Api

This API is built to simply the logging of player actions inside a minecraft server.  It should allow plugin developers to implement a connection to the API to reduce complexity of the application, leading to faster release times after each new MC version is released.

## How This Works

This is an evolving API and offers no promise of stability at this time.  Every effort will be made to minimize API shattering changes between each version.

## API Versions

No set versioning scheme has been chosen at this time.

## Documentation

TODO

## How Does the Goo API work?

The API is built on node.js

You can install node.js with the directions written by [Joyent](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager)

After cloning this repo you should be able to cd into the directory where app.js is located.

```
npm install
```

TODO: Add supervisor instructions


```
node app.js
```

At this point you should be able to navigate to [localhost:3001/api/](http://localhost:3001/api) and see a response indicating that the server is running.


