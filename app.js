
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , block = require('./routes/block')
  , api = require('./routes/api')
  , player = require('./routes/player')
  , http = require('http')
  , path = require('path');

var app = express();
var port = (process.env.VMC_APP_PORT || 3001);
var host = (process.env.VCAP_APP_HOST || 'localhost');

//Set up Mongo
if(process.env.VCAP_SERVICES){
  var env = JSON.parse(process.env.VCAP_SERVICES);
  var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
  var mongo = {
    "hostname":"localhost",
    "port":27017,
    "username":"",
    "password":"",
    "name":"",
    "db":"db"
  }
}

var generate_mongo_url = function(obj){
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'test');

  if(obj.username && obj.password){
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
  else{
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
}

var mongourl = generate_mongo_url(mongo);



// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


//gets
app.get('/', api.authenticate);
app.get('/players', player.list);
app.get('/players/:player_id', player.findByPlayerName);
app.get('/players/id/:id', player.findById);
app.get('/players/:player_id/blocks', block.findByPlayerId);
app.get('/players/:player_id/blocks/summary', block.summaryByPlayerId);
app.get('/blocks', block.list);
app.get('/blocks/:block_id', block.findById);
//Posts
app.post('/players', player.add);
app.post('/blocks', block.add);
app.put('/blocks',block.stuffItIn)

app.listen(port, host);
console.log("Express server listening on port %d in %s mode", port, app.settings.env);
console.log("Mongodb listening on port %d", mongo['port']);
